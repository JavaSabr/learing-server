package com.instinctools.travel.guide.db.hibernate.entity.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The entity to contains information about image interest points.
 *
 * @author Alex Brui
 */
@Entity
@Table(name = "interest_point")
public class InterestPoint extends BaseEntity {

    /**
     * The reference to image data.
     */
    @ManyToOne(targetEntity = ImageData.class, fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id", name = "image_data_id", nullable = false)
    public ImageData imageData;

    /**
     * The x-coordinate of the interest point.
     */
    @Column(name = "x", nullable = false)
    private float x;

    /**
     * The y-coordinate of the interest point.
     */
    @Column(name = "y", nullable = false)
    private float y;

    /**
     * The diameter of the interest point.
     */
    @Column(name = "size", nullable = false)
    private float size;

    /**
     * The orientation of the interest point.
     */
    @Column(name = "angle", nullable = false)
    private float angle;

    /**
     * The detector response on the interest point.
     */
    @Column(name = "response", nullable = false)
    private float response;

    /**
     * The pyramid octave in which the interest point has been detected.
     */
    @Column(name = "octave", nullable = false)
    private int octave;

    public InterestPoint() {
    }

    public InterestPoint(@NotNull final ImageData imageData, final float x, final float y, final float size, final float angle, final float response,
                         final int octave) {
        this.imageData = imageData;
        this.x = x;
        this.y = y;
        this.size = size;
        this.angle = angle;
        this.response = response;
        this.octave = octave;
    }

    public InterestPoint(@Nullable final Long id, @NotNull final ImageData imageData, final float x, final float y,
                         final float size, final float angle, final float response, final int octave) {
        super(id);
        this.imageData = imageData;
        this.x = x;
        this.y = y;
        this.size = size;
        this.angle = angle;
        this.response = response;
        this.octave = octave;
    }

    /**
     * @return the reference to image data.
     */
    @NotNull
    public ImageData getImageData() {
        return imageData;
    }

    /**
     * @param imageData the reference to image data.
     */
    public void setImageData(@NotNull final ImageData imageData) {
        this.imageData = imageData;
    }

    /**
     * @return the x-coordinate of the interest point.
     */
    public float getX() {
        return x;
    }

    /**
     * @param x the x-coordinate of the interest point.
     */
    public void setX(final float x) {
        this.x = x;
    }

    /**
     * @return the y-coordinate of the interest point.
     */
    public float getY() {
        return y;
    }

    /**
     * @param y the y-coordinate of the interest point.
     */
    public void setY(final float y) {
        this.y = y;
    }

    /**
     * @return the orientation of the interest point.
     */
    public float getAngle() {
        return angle;
    }

    /**
     * @param angle the orientation of the interest point.
     */
    public void setAngle(final float angle) {
        this.angle = angle;
    }

    /**
     * @return the detector response on the interest point.
     */
    public float getResponse() {
        return response;
    }

    /**
     * @param response the detector response on the interest point.
     */
    public void setResponse(final float response) {
        this.response = response;
    }

    /**
     * @return the pyramid octave in which the interest point has been detected.
     */
    public int getOctave() {
        return octave;
    }

    /**
     * @param octave the pyramid octave in which the interest point has been detected.
     */
    public void setOctave(final int octave) {
        this.octave = octave;
    }

    /**
     * @return the diameter of the interest point.
     */
    public float getSize() {
        return size;
    }

    /**
     * @param size the diameter of the interest point.
     */
    public void setSize(final float size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "InterestPoint{" +
                "x=" + x +
                ", y=" + y +
                ", size=" + size +
                ", angle=" + angle +
                ", response=" + response +
                ", octave=" + octave +
                "} " + super.toString();
    }
}
