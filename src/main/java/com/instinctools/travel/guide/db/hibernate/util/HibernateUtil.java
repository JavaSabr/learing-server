package com.instinctools.travel.guide.db.hibernate.util;

import com.instinctools.travel.guide.service.impl.HibernateService;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * The utility class.
 *
 * @author JavaSaBr
 */
public class HibernateUtil {

    @NotNull
    public static final HibernateService HIBERNATE_SERVICE = HibernateService.getInstance();

    /**
     * Close a session.
     *
     * @param session the session.
     */
    public static void close(@Nullable final Session session) {
        if (session == null) return;
        session.close();
    }

    /**
     * Rollback a transaction.
     *
     * @param transaction the transaction.
     */
    public static void rollback(@Nullable final Transaction transaction) {
        if (transaction == null) return;
        transaction.rollback();
    }

    /**
     * Open a new session.
     *
     * @return the new session.
     */
    @NotNull
    public static Session openSession() {
        final SessionFactory sessionFactory = HIBERNATE_SERVICE.getSessionFactory();
        return sessionFactory.openSession();
    }
}
