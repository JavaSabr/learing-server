package com.instinctools.travel.guide.db.hibernate.entity.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Blob;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The base entity to store an image data.
 *
 * @author Alex Brui
 */
@Entity
@Table(name = "image_data")
public class ImageData extends BaseEntity {

    /**
     * The reference to searchable image.
     */
    @ManyToOne(targetEntity = SearchableObject.class, fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id", name = "searchable_object", nullable = false)
    public SearchableObject object;

    /**
     * The data of an image.
     */
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data")
    protected Blob data;

    /**
     * The description to match.
     */
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "description")
    protected byte[] description;

    public ImageData() {
    }

    public ImageData(@Nullable final Long id, @NotNull final SearchableObject object) {
        super(id);
        this.object = object;
    }

    /**
     * Get an image data.
     *
     * @return the image data.
     */
    @NotNull
    public Blob getData() {
        return data;
    }

    /**
     * Set an image data.
     *
     * @param data the image data.
     */
    public void setData(@NotNull final Blob data) {
        this.data = data;
    }

    /**
     * @return the description to match.
     */
    @NotNull
    public byte[] getDescription() {
        return description;
    }

    /**
     * @param description the description to match.
     */
    public void setDescription(@NotNull final byte[] description) {
        this.description = description;
    }

    /**
     * @return the reference to searchable image.
     */
    @NotNull
    public SearchableObject getObject() {
        return object;
    }

    /**
     * @param object the reference to searchable image.
     */
    public void setObject(@NotNull final SearchableObject object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "ImageData{" +
                "object=" + object +
                "} " + super.toString();
    }
}
