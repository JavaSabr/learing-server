package com.instinctools.travel.guide.db.hibernate.entity.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The class to contains information about a searchable image.
 *
 * @author Alex Brui
 */
@Entity
@Table(name = "searchable_object")
public class SearchableObject extends BaseEntity {

    /**
     * The name of an object.
     */
    @Column(name = "name")
    private String name;

    /**
     * The description of an object.
     */
    @Column(name = "description")
    private String description;

    public SearchableObject() {
    }

    public SearchableObject(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public SearchableObject(@Nullable final Long id, @NotNull final String name, @NotNull final String description) {
        super(id);
        this.name = name;
        this.description = description;
    }

    /**
     * @return the description of an object.
     */
    @NotNull
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description of an object.
     */
    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    /**
     * @return the name of an object.
     */
    @NotNull
    public String getName() {
        return name;
    }

    /**
     * @param name the name of an object.
     */
    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SearchableObject{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                "} " + super.toString();
    }
}
