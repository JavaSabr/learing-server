package com.instinctools.travel.guide.util;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by ronn on 12.01.17.
 */
public class Utils {

    public static void deleteIfExists(@Nullable final Path file) {
        if (file == null) return;
        try {
            Files.deleteIfExists(file);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }
}
