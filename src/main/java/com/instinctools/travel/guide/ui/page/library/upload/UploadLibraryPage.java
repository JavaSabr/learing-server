package com.instinctools.travel.guide.ui.page.library.upload;

import static java.util.stream.Collectors.toList;

import com.instinctools.travel.guide.db.hibernate.entity.impl.ImageData;
import com.instinctools.travel.guide.db.hibernate.entity.impl.SearchableObject;
import com.instinctools.travel.guide.service.impl.ImageDataService;
import com.instinctools.travel.guide.service.impl.InterestPointService;
import com.instinctools.travel.guide.service.impl.SearchableObjectService;
import com.instinctools.travel.guide.util.Utils;
import com.vaadin.annotations.Theme;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * The implementation an upload library page.
 *
 * @author Alex Brui
 */
@Theme("mytheme")
public class UploadLibraryPage extends UI {

    /**
     * The searchable object service.
     */
    @NotNull
    private final SearchableObjectService objectService;

    /**
     * The image data service.
     */
    @NotNull
    private final ImageDataService imageDataService;

    /**
     * The service to work with interest points.
     */
    @NotNull
    private final InterestPointService interestPointService;

    /**
     * The list of uploaded files.
     */
    @NotNull
    private final Map<String, Path> uploadingFiles;

    /**
     * The map with names of succeeded uploaded files.
     */
    @NotNull
    private final Map<String, Boolean> uploadedFiles;

    /**
     * The layout to contain upload components.
     */
    private VerticalLayout uploadLayout;

    /**
     * The field with object name.
     */
    private TextField nameField;

    /**
     * The text area with object description.
     */
    private TextArea descriptionField;

    /**
     * The button to create searchable object.
     */
    private Button createObjectButton;

    public UploadLibraryPage() {
        this.objectService = SearchableObjectService.getInstance();
        this.imageDataService = ImageDataService.getInstance();
        this.interestPointService = InterestPointService.getInstance();
        this.uploadingFiles = new HashMap<>();
        this.uploadedFiles = new HashMap<>();
    }

    @Override
    protected void init(@NotNull final VaadinRequest vaadinRequest) {

        nameField = new TextField();
        nameField.setCaption("Object name");
        nameField.addValueChangeListener(event -> validate());

        descriptionField = new TextArea();
        descriptionField.setCaption("Description");
        descriptionField.addValueChangeListener(event -> validate());

        createObjectButton = new Button();
        createObjectButton.setCaption("Create object");
        createObjectButton.addClickListener(this::createSearchableObject);

        final VerticalLayout inputContainer = new VerticalLayout();
        inputContainer.addComponent(nameField);
        inputContainer.addComponent(descriptionField);
        inputContainer.setMargin(true);
        inputContainer.setSpacing(true);

        final HorizontalLayout inputAndButtonContainer = new HorizontalLayout();
        inputAndButtonContainer.addComponent(inputContainer);
        inputAndButtonContainer.addComponent(createObjectButton);
        inputAndButtonContainer.setMargin(true);

        final Upload upload = new Upload("Image:", this::handleFile);
        upload.addSucceededListener(this::handleUploadedFile);

        final Label label = new Label("Uploaded images:");

        uploadLayout = new VerticalLayout();
        uploadLayout.setMargin(true);
        uploadLayout.setSpacing(true);
        uploadLayout.addComponent(upload);
        uploadLayout.addComponent(label);

        final VerticalLayout mainContainer = new VerticalLayout();
        mainContainer.addComponent(inputAndButtonContainer);
        mainContainer.addComponent(uploadLayout);
        mainContainer.setMargin(true);
        mainContainer.setSpacing(true);

        validate();

        setContent(mainContainer);
    }

    /**
     * Handle an uploaded file.
     */
    private void handleUploadedFile(final Upload.SucceededEvent succeededEvent) {

        final String filename = succeededEvent.getFilename();

        uploadedFiles.put(filename, Boolean.TRUE);
        uploadLayout.addComponent(new Image(filename,
                new FileResource(uploadingFiles.get(filename).toFile())));

        validate();
    }

    /**
     * Validate input.
     */
    private void validate() {

        final String name = nameField.getValue();
        final String description = descriptionField.getValue();

        createObjectButton.setEnabled(!uploadedFiles.isEmpty() &&
                StringUtils.isNotEmpty(name) &&
                StringUtils.isNotEmpty(description));
    }

    /**
     * Create a searchable object.
     */
    private void createSearchableObject(@NotNull final Button.ClickEvent event) {

        final String name = nameField.getValue();
        final String description = descriptionField.getValue();

        final SearchableObject searchableObject = objectService.create(name, description);

        uploadingFiles.forEach((s, path) -> createImageData(searchableObject, s, path));

        clear();
    }

    /**
     * Clear a state of this page.
     */
    private void clear() {

        nameField.setValue("");
        descriptionField.setValue("");

        IntStream.range(0, uploadLayout.getComponentCount())
                .mapToObj(uploadLayout::getComponent)
                .filter(component -> component instanceof Image)
                .collect(toList())
                .forEach(uploadLayout::removeComponent);


        uploadingFiles.forEach((filename, path) -> Utils.deleteIfExists(path));
        uploadingFiles.clear();
        uploadedFiles.clear();
    }

    private void createImageData(@NotNull final SearchableObject searchableObject, @NotNull final String filename,
                                 @NotNull final Path path) {
        if (!uploadedFiles.containsKey(filename)) return;
        final ImageData imageData = imageDataService.createFor(path, searchableObject);
        interestPointService.createFor(path, imageData);
    }

    @NotNull
    private OutputStream handleFile(@NotNull final String filename, @NotNull final String mimeType) {
        if (StringUtils.isEmpty(filename)) throw new RuntimeException();
        try {
            final Path tempFile = Files.createTempFile("upload_image", filename);
            uploadingFiles.put(filename, tempFile);
            return Files.newOutputStream(tempFile);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void detach() {
        clear();
        super.detach();
    }
}
