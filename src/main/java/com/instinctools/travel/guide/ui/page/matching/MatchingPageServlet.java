package com.instinctools.travel.guide.ui.page.matching;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import javax.servlet.annotation.WebServlet;

/**
 * @author Alex Brui
 */
@WebServlet(urlPatterns = "/matching/*", name = "MatchingPageServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = MatchingPage.class, productionMode = false)
public class MatchingPageServlet extends VaadinServlet {
}
