package com.instinctools.travel.guide.ui.component;

import com.instinctools.travel.guide.db.hibernate.entity.impl.ImageData;
import com.instinctools.travel.guide.db.hibernate.entity.impl.SearchableObject;
import com.instinctools.travel.guide.service.impl.ImageDataService;
import com.instinctools.travel.guide.util.Utils;
import com.vaadin.server.FileResource;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The library element component.
 *
 * @author Alex Brui
 */
public class LibraryElementComponent extends VerticalLayout {

    /**
     * The image data service.
     */
    @NotNull
    private final ImageDataService imageDataService;

    /**
     * The searchable object.
     */
    @NotNull
    private final SearchableObject object;

    /**
     * The image files.
     */
    private List<Path> imageFiles;

    /**
     * The image components.
     */
    private List<Image> images;

    public LibraryElementComponent(@NotNull final SearchableObject object) {
        this.imageDataService = ImageDataService.getInstance();
        this.object = object;
        this.images = new ArrayList<>();
        this.imageFiles = new ArrayList<>();
        createComponents();
    }

    protected void createComponents() {

        final SearchableObject object = getObject();
        final List<ImageData> imageData = imageDataService.getImageData(object);

        final Label nameLabel = new Label(object.getName());
        final VerticalLayout container = new VerticalLayout();
        final AtomicInteger index = new AtomicInteger(0);

        imageData.forEach(data -> {

            final Path preparedImage = prepareImage(data);

            final Image image = new Image("image#" + index.incrementAndGet(), new FileResource(preparedImage.toFile()));
            image.setWidth(256, Unit.PIXELS);
            image.setHeight(256, Unit.PIXELS);

            images.add(image);

            container.addComponent(image);
        });

        addComponent(nameLabel);
        addComponent(container);
    }

    @NotNull
    public SearchableObject getObject() {
        return Objects.requireNonNull(object, "Object can't be null.");
    }

    @NotNull
    protected Path prepareImage(@NotNull final ImageData imageData) {

        final ImageDataService imageDataService = ImageDataService.getInstance();

        final Path result;
        try {

            result = Files.createTempFile("img_lib_el", object.getName());

            final Blob data = imageData.getData();

            Files.copy(data.getBinaryStream(), result, StandardCopyOption.REPLACE_EXISTING);

            imageFiles.add(result);

        } catch (final IOException | SQLException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    @Override
    public void detach() {
        if (imageFiles != null) {
            imageFiles.forEach(Utils::deleteIfExists);
        }
        super.detach();
    }
}
