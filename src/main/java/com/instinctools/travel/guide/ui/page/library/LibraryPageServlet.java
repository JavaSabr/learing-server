package com.instinctools.travel.guide.ui.page.library;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import javax.servlet.annotation.WebServlet;

/**
 * The implementation a library page servlet.
 *
 * @author Alex Brui
 */
@WebServlet(urlPatterns = "/library/*", name = "LibraryPageServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = LibraryPage.class, productionMode = false)
public class LibraryPageServlet extends VaadinServlet {
}
