package com.instinctools.travel.guide.ui.component;

import com.instinctools.travel.guide.db.hibernate.entity.impl.SearchableObject;
import com.instinctools.travel.guide.service.impl.SearchableObjectService;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

import java.util.List;

/**
 * The library component.
 *
 * @author Alex Brui
 */
public class LibraryComponent extends VerticalLayout {

    public LibraryComponent() {
        createComponents();
    }

    private void createComponents() {

        final HorizontalLayout elementContainer = new HorizontalLayout();

        final SearchableObjectService objectService = SearchableObjectService.getInstance();
        final List<SearchableObject> allObjects = objectService.getAllObjects();

        allObjects.forEach(object -> elementContainer.addComponent(new LibraryElementComponent(object)));

        addComponent(elementContainer);
    }
}
