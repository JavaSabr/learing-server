package com.instinctools.travel.guide.ui;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import javax.servlet.annotation.WebServlet;

/**
 * The main servlet.
 *
 * @author Alex Brui
 */
@WebServlet(urlPatterns = "/*", name = "TravelGuideApplicationServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = TravelGuideApplication.class, productionMode = false)
public class TravelGuideApplicationServlet extends VaadinServlet {
}
