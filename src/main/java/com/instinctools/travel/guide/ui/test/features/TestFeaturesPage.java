package com.instinctools.travel.guide.ui.test.features;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;

import com.instinctools.travel.guide.opencv.test.features.FASTTest;
import com.instinctools.travel.guide.opencv.test.features.GFTTTest;
import com.instinctools.travel.guide.opencv.test.features.HarrisTest;
import com.instinctools.travel.guide.opencv.test.features.SIFTTest;
import com.instinctools.travel.guide.opencv.test.features.SURFTest;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Image;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

/**
 * @author Alex Brui
 */
@Theme("mytheme")
public class TestFeaturesPage extends UI {

    @Override
    protected void init(@NotNull final VaadinRequest vaadinRequest) {

        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);

        layout.addComponent(new Image("Harris",
                new FileResource(BASE_IMAGE_PATH.resolve("test-features/harris-dest.jpg").toFile())));

        layout.addComponent(new Image("Harris",
                new FileResource(BASE_IMAGE_PATH.resolve("test-features/harris-dest2.jpg").toFile())));

        layout.addComponent(new Image("GFTT",
                new FileResource(BASE_IMAGE_PATH.resolve("test-features/gftt-dest.jpg").toFile())));

        layout.addComponent(new Image("FAST",
                new FileResource(BASE_IMAGE_PATH.resolve("test-features/fast-dest.jpg").toFile())));

        layout.addComponent(new Image("SURF",
                new FileResource(BASE_IMAGE_PATH.resolve("test-features/surf-dest.jpg").toFile())));

        layout.addComponent(new Image("SIFT",
                new FileResource(BASE_IMAGE_PATH.resolve("test-features/sift-dest.jpg").toFile())));

        setContent(layout);
    }

    @WebServlet(urlPatterns = "/test-features/*", name = "TestFeaturesPageServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = TestFeaturesPage.class, productionMode = false)
    public static class TestFeaturesPageServlet extends VaadinServlet {

        @Override
        public void init() throws ServletException {
            super.init();

            try {
                Files.createDirectories(BASE_IMAGE_PATH.resolve("test-features"));
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }

            HarrisTest.test();
            GFTTTest.test();
            FASTTest.test();
            SURFTest.test();
            SIFTTest.test();
        }
    }
}
