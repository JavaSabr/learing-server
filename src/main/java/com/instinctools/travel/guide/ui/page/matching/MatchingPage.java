package com.instinctools.travel.guide.ui.page.matching;

import static com.instinctools.travel.guide.opencv.util.JavaCVUtils.add;
import static com.instinctools.travel.guide.opencv.util.JavaCVUtils.*;
import static java.util.Arrays.asList;
import static org.bytedeco.javacpp.opencv_calib3d.RANSAC;
import static org.bytedeco.javacpp.opencv_calib3d.findHomography;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags.NOT_DRAW_SINGLE_POINTS;
import static org.bytedeco.javacpp.opencv_features2d.drawMatches;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.line;
import static org.bytedeco.javacpp.opencv_xfeatures2d.SURF.create;
import com.instinctools.travel.guide.db.hibernate.entity.impl.ImageData;
import com.instinctools.travel.guide.db.hibernate.entity.impl.SearchableObject;
import com.instinctools.travel.guide.opencv.util.JavaCVUtils;
import com.instinctools.travel.guide.service.impl.ImageDataService;
import com.instinctools.travel.guide.service.impl.SearchableObjectService;
import com.instinctools.travel.guide.util.Utils;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Property;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import org.apache.commons.lang3.StringUtils;
import org.bytedeco.javacpp.opencv_core.*;
import org.bytedeco.javacpp.opencv_features2d;
import org.bytedeco.javacpp.opencv_features2d.BFMatcher;
import org.bytedeco.javacpp.opencv_features2d.Feature2D;
import org.bytedeco.javacpp.opencv_features2d.FlannBasedMatcher;
import org.bytedeco.javacpp.opencv_features2d.ORB;
import org.bytedeco.javacpp.opencv_xfeatures2d;
import org.bytedeco.javacpp.opencv_xfeatures2d.SIFT;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Alex Brui
 */
@Theme("mytheme")
public class MatchingPage extends UI {

    private static final Scalar GREEN_COLOR = new Scalar(0, 255, 0, 0);

    private GridLayout surfSettings;
    private GridLayout obrSettings;
    private GridLayout siftSettings;

    private VerticalLayout root;
    private VerticalLayout compareContainer;
    private VerticalLayout uploadedContainer;

    private TextField surfHessianThresholdField;
    private TextField surfNOctavesField;
    private TextField surfNOctaveLayersField;

    private TextField obrNFeaturesField;
    private TextField obrScaleFactorField;
    private TextField obrNLevelsField;
    private TextField obrEdgeThresholdField;
    private TextField obrFirstLevelField;
    private TextField obrWtaKField;
    private TextField obrPatchSizeField;
    private TextField obrFastThresholdField;

    private TextField siftNFeaturesField;
    private TextField siftNOctaveLayersField;
    private TextField siftContrastThresholdField;
    private TextField siftEdgeThresholdField;
    private TextField siftSigmaField;

    private CheckBox surfExtendedBox;
    private CheckBox surfUprightBox;

    private ComboBox featureTypeBox;
    private ComboBox matchTypeBox;

    private Button matchButton;

    private Path uploadedFile;

    private boolean uploaded;

    public MatchingPage() {
    }

    @Override
    protected void init(@NotNull final VaadinRequest vaadinRequest) {

        final GridLayout settings = new GridLayout(3, 1);
        settings.setMargin(true);
        settings.setSpacing(true);
        settings.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        root = new VerticalLayout();

        createSURFSettings();
        createOBRSettings();
        createSIFTSettings();

        matchTypeBox = new ComboBox("Match type", asList("BFMatcher", "FlannBasedMatcher"));
        matchTypeBox.setValue("FlannBasedMatcher");

        featureTypeBox = new ComboBox("Feature type", asList("SURF", "OBR", "SIFT"));
        featureTypeBox.addValueChangeListener(this::switchSettings);

        matchButton = new Button("Match");
        matchButton.addClickListener(this::startMatching);

        final Upload upload = new Upload("Upload", this::handleFile);
        upload.addSucceededListener(this::handleUploadedFile);

        compareContainer = new VerticalLayout();

        uploadedContainer = new VerticalLayout();
        uploadedContainer.setMargin(true);
        uploadedContainer.setSpacing(true);

        settings.addComponent(matchTypeBox, 0, 0);
        settings.addComponent(featureTypeBox, 1, 0);
        settings.addComponent(matchButton, 2, 0);

        root.addComponent(settings);
        root.addComponent(upload);
        root.addComponent(uploadedContainer);
        root.addComponent(compareContainer);

        validate();
        setContent(root);

        featureTypeBox.setValue("SURF");
    }

    private void createOBRSettings() {

        obrSettings = new GridLayout(4, 2);
        obrSettings.setMargin(true);
        obrSettings.setSpacing(true);

        obrNFeaturesField = new TextField("N features", "500");
        obrScaleFactorField = new TextField("Scale factor", "1.2");
        obrNLevelsField = new TextField("N levels", "8");
        obrEdgeThresholdField = new TextField("Edge threshold", "31");
        obrFirstLevelField = new TextField("First level", "0");
        obrWtaKField = new TextField("WTA K", "2");
        obrPatchSizeField = new TextField("Patch size", "31");
        obrFastThresholdField = new TextField("Fast threshold", "20");

        obrSettings.addComponent(obrNFeaturesField, 0, 0);
        obrSettings.addComponent(obrScaleFactorField, 1, 0);
        obrSettings.addComponent(obrNLevelsField, 2, 0);
        obrSettings.addComponent(obrEdgeThresholdField, 3, 0);
        obrSettings.addComponent(obrFirstLevelField, 0, 1);
        obrSettings.addComponent(obrWtaKField, 1, 1);
        obrSettings.addComponent(obrPatchSizeField, 2, 1);
        obrSettings.addComponent(obrFastThresholdField, 3, 1);
    }

    private void createSIFTSettings() {

        siftSettings = new GridLayout(3, 2);
        siftSettings.setMargin(true);
        siftSettings.setSpacing(true);

        siftNFeaturesField = new TextField("N features", "0");
        siftNOctaveLayersField = new TextField("N octave layers", "3");
        siftContrastThresholdField = new TextField("Contrast threshold", "0.04");
        siftEdgeThresholdField = new TextField("Edge threshold", "10");
        siftSigmaField = new TextField("Sigma", "1.6");

        siftSettings.addComponent(siftNFeaturesField, 0, 0);
        siftSettings.addComponent(siftNOctaveLayersField, 1, 0);
        siftSettings.addComponent(siftContrastThresholdField, 2, 0);
        siftSettings.addComponent(siftEdgeThresholdField, 0, 1);
        siftSettings.addComponent(siftSigmaField, 1, 1);
    }

    private void createSURFSettings() {

        surfSettings = new GridLayout(3, 2);
        surfSettings.setMargin(true);
        surfSettings.setSpacing(true);

        surfHessianThresholdField = new TextField("Hessian threshold", "2500");
        surfNOctavesField = new TextField("N octaves", "4");
        surfNOctaveLayersField = new TextField("N octave layers", "2");

        surfExtendedBox = new CheckBox("Extended", true);
        surfUprightBox = new CheckBox("Upright", false);

        surfSettings.addComponent(surfHessianThresholdField, 0, 0);
        surfSettings.addComponent(surfNOctavesField, 1, 0);
        surfSettings.addComponent(surfNOctaveLayersField, 2, 0);
        surfSettings.addComponent(surfExtendedBox, 0, 1);
        surfSettings.addComponent(surfUprightBox, 1, 1);
    }

    private void switchSettings(@NotNull final Property.ValueChangeEvent event) {

        final Property property = event.getProperty();
        final Object value = property.getValue();

        root.removeComponent(surfSettings);
        root.removeComponent(obrSettings);
        root.removeComponent(siftSettings);

        if ("SURF".equals(value)) {
            root.addComponent(surfSettings, 1);
        } else if ("OBR".equals(value)) {
            root.addComponent(obrSettings, 1);
        } else if ("SIFT".equals(value)) {
            root.addComponent(siftSettings, 1);
        }
    }

    private void startMatching(@NotNull final Button.ClickEvent event) {
        compareContainer.removeAllComponents();

        final BFMatcher bfMatcher = new BFMatcher(NORM_L2, false);
        final FlannBasedMatcher flannBasedMatcher = new FlannBasedMatcher();

        final String matchType = (String) matchTypeBox.getValue();
        final String featureType = (String) featureTypeBox.getValue();

        final Feature2D featureDetector = createFeatureDetector(featureType);

        final KeyPointVector targetPoints = new KeyPointVector();

        final Mat target = imread(uploadedFile.toString(), IMREAD_GRAYSCALE);
        final Mat targetDescription = new Mat();

        featureDetector.detect(target, targetPoints);
        featureDetector.compute(target, targetPoints, targetDescription);

        final SearchableObjectService objectService = SearchableObjectService.getInstance();
        final ImageDataService imageDataService = ImageDataService.getInstance();

        final List<SearchableObject> allObjects = objectService.getAllObjects();

        for (final SearchableObject searchableObject : allObjects) {

            final List<ImageData> imageData = imageDataService.getImageData(searchableObject);

            for (final ImageData data : imageData) {
                try {

                    final HorizontalLayout matchResultContainer = new HorizontalLayout();

                    final Blob sourceImage = data.getData();
                    final Path sourceFile = Files.createTempFile("matching", "source");

                    Files.copy(sourceImage.getBinaryStream(), sourceFile, StandardCopyOption.REPLACE_EXISTING);

                    final Mat source = imread(sourceFile.toString(), IMREAD_GRAYSCALE);
                    final KeyPointVector sourcePoints = new KeyPointVector();

                    featureDetector.detect(source, sourcePoints);

                    final Mat sourceDescription = new Mat();

                    featureDetector.compute(source, sourcePoints, sourceDescription);

                    final DMatchVector matches = new DMatchVector();

                    if ("BFMatcher".endsWith(matchType)) {
                        bfMatcher.match(sourceDescription, targetDescription, matches);
                    } else {
                        flannBasedMatcher.match(sourceDescription, targetDescription, matches);
                    }

                    double maxDist = Integer.MIN_VALUE;
                    double minDist = Integer.MAX_VALUE;

                    //-- Quick calculation of max and min distances between keypoints
                    for (int i = 0; i < sourceDescription.rows(); i++) {
                        double dist = matches.get(i).distance();
                        if (dist < minDist) minDist = dist;
                        if (dist > maxDist) maxDist = dist;
                    }

                    final double resultDistance = 3 * maxDist;

                    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
                    final DMatchVector goodMatches = JavaCVUtils.selectBest(matches, dMatch -> true, 25);

                    // Draw best matches
                    final Mat imageMatches = new Mat();

                    drawMatches(source, sourcePoints, target, targetPoints, goodMatches, imageMatches,
                            new Scalar(0, 0, 255, 0), new Scalar(255, 0, 0, 0), (ByteBuffer) null, NOT_DRAW_SINGLE_POINTS);

                    //-- Localize the object
                    final Point2fVector[] point2fVectors = JavaCVUtils.toPoint2fVectorPair(goodMatches, sourcePoints, targetPoints);
                    final Point2fVector sourceVPoints = point2fVectors[0];
                    final Point2fVector targetVPoints = point2fVectors[1];

                    // Find the homography between image 1 and image 2
                    final Mat inliers = new Mat();
                    final Mat homography = findHomography(toMat(sourceVPoints), toMat(targetVPoints), // corresponding points
                            inliers, // outputted inliers matches
                            RANSAC, // RANSAC method
                            1.0 // max distance to reprojection point
                    );

                    //-- Get the corners from the image_1 ( the object to be "detected" )
                    final Point2fVector objCorners = new Point2fVector(new Point2f(0, 0), new Point2f(source.cols(), 0),
                            new Point2f(source.cols(), source.rows()), new Point2f(0, source.rows()));

                    final Mat sceneCornersMat = new Mat();

                    perspectiveTransform(toMat(objCorners), sceneCornersMat, homography);

                    final Point2fVector sceneCorners = toPointVector(sceneCornersMat);

                    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
                    line(imageMatches, add(sceneCorners.get(0), new Point2f(source.cols(), 0)), add(sceneCorners.get(1),
                            new Point2f(source.cols(), 0)), GREEN_COLOR);

                    line(imageMatches, add(sceneCorners.get(1), new Point2f(source.cols(), 0)), add(sceneCorners.get(2),
                            new Point2f(source.cols(), 0)), GREEN_COLOR);

                    line(imageMatches, add(sceneCorners.get(2), new Point2f(source.cols(), 0)), add(sceneCorners.get(3),
                            new Point2f(source.cols(), 0)), GREEN_COLOR);

                    line(imageMatches, add(sceneCorners.get(3), new Point2f(source.cols(), 0)), add(sceneCorners.get(0),
                            new Point2f(source.cols(), 0)), GREEN_COLOR);

                    final Path matching = Files.createTempFile("matching", ".jpg");

                    imwrite(matching.toString(), imageMatches);

                    final Image result = new Image("#", new FileResource(matching.toFile()));
                    result.setWidth(512, Unit.PIXELS);
                    result.setHeight(512, Unit.PIXELS);

                    matchResultContainer.addComponent(result);

                    compareContainer.addComponent(matchResultContainer);

                } catch (final SQLException | IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @NotNull
    private Feature2D createFeatureDetector(@NotNull final String featureType) {

        switch (featureType) {
            case "SURF": {

                final double hessianThreshold = Double.parseDouble(surfHessianThresholdField.getValue());
                final int nOctaves = Integer.parseInt(surfNOctavesField.getValue());
                final int nOctaveLayers = Integer.parseInt(surfNOctaveLayersField.getValue());

                return create(hessianThreshold, nOctaves, nOctaveLayers, surfExtendedBox.getValue(),
                        surfUprightBox.getValue());
            }
            case "OBR": {

                final int nfeatures = Integer.parseInt(obrNFeaturesField.getValue());
                final float scaleFactor = Float.parseFloat(obrScaleFactorField.getValue());
                final int nlevels = Integer.parseInt(obrNLevelsField.getValue());
                final int edgeThreshold = Integer.parseInt(obrEdgeThresholdField.getValue());
                final int firstLevel = Integer.parseInt(obrFirstLevelField.getValue());
                final int wtaK = Integer.parseInt(obrWtaKField.getValue());
                final int patchSize = Integer.parseInt(obrPatchSizeField.getValue());
                final int fastThreshold = Integer.parseInt(obrFastThresholdField.getValue());

                return ORB.create(nfeatures, scaleFactor, nlevels, edgeThreshold, firstLevel, wtaK, ORB.HARRIS_SCORE,
                        patchSize, fastThreshold);
            }
            case "SIFT": {

                final int nfeatures = Integer.parseInt(siftNFeaturesField.getValue());
                final int nOctaveLayers = Integer.parseInt(siftNOctaveLayersField.getValue());
                final double contrastThreshold = Double.parseDouble(siftContrastThresholdField.getValue());
                final double edgeThreshold = Double.parseDouble(siftEdgeThresholdField.getValue());
                final double sigma = Double.parseDouble(siftSigmaField.getValue());

                return SIFT.create(nfeatures, nOctaveLayers, contrastThreshold, edgeThreshold, sigma);
            }
        }

        throw new RuntimeException(featureType);
    }

    /**
     * Handle an uploaded file.
     */
    private void handleUploadedFile(final Upload.SucceededEvent succeededEvent) {
        uploaded = true;
        uploadedContainer.removeAllComponents();
        uploadedContainer.addComponent(new Image("Uploaded", new FileResource(uploadedFile.toFile())));
        validate();
    }

    private void validate() {
        matchButton.setEnabled(uploadedFile != null && uploaded);
    }

    @NotNull
    private OutputStream handleFile(@NotNull final String filename, @NotNull final String mimeType) {
        if (StringUtils.isEmpty(filename)) throw new RuntimeException();
        try {
            uploaded = false;
            final Path tempFile = Files.createTempFile("upload_image", filename);
            uploadedFile = tempFile;
            return Files.newOutputStream(tempFile);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void detach() {
        Utils.deleteIfExists(uploadedFile);
        super.detach();
    }
}
