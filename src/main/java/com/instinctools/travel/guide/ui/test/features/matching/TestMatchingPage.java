package com.instinctools.travel.guide.ui.test.features.matching;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;

import com.instinctools.travel.guide.opencv.test.features.matching.SURFMatchingTest;
import com.instinctools.travel.guide.opencv.test.features.matching.TemplateMatchingTest;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Image;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

/**
 * @author Alex Brui
 */
@Theme("mytheme")
public class TestMatchingPage extends UI {

    @Override
    protected void init(@NotNull final VaadinRequest vaadinRequest) {

        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);

        layout.addComponent(new Image("Template: source template",
                new FileResource(BASE_IMAGE_PATH.resolve("source.jpg").toFile())));

        layout.addComponent(new Image("Template: result template",
                new FileResource(BASE_IMAGE_PATH.resolve("test-matching/template.jpg").toFile())));

        layout.addComponent(new Image("Template: other photo",
                new FileResource(BASE_IMAGE_PATH.resolve("source-2.jpg").toFile())));

        layout.addComponent(new Image("Template: ROI-1",
                new FileResource(BASE_IMAGE_PATH.resolve("test-matching/roi-1.jpg").toFile())));

        layout.addComponent(new Image("Template: ROI-2",
                new FileResource(BASE_IMAGE_PATH.resolve("test-matching/roi-2.jpg").toFile())));

        layout.addComponent(new Image("SURF: image 1",
                new FileResource(BASE_IMAGE_PATH.resolve("source.jpg").toFile())));

        layout.addComponent(new Image("SURF: image 2",
                new FileResource(BASE_IMAGE_PATH.resolve("source-2.jpg").toFile())));

        layout.addComponent(new Image("SURF: compared",
                new FileResource(BASE_IMAGE_PATH.resolve("test-matching/surf-result.jpg").toFile())));

        setContent(layout);
    }

    @WebServlet(urlPatterns = "/test-matching/*", name = "TestMatchingPageServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = TestMatchingPage.class, productionMode = false)
    public static class TestMatchingPageServlet extends VaadinServlet {

        @Override
        public void init() throws ServletException {
            super.init();

            try {
                Files.createDirectories(BASE_IMAGE_PATH.resolve("test-matching/"));
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }

            TemplateMatchingTest.test();
            SURFMatchingTest.test();
        }
    }
}
