package com.instinctools.travel.guide.ui.page.library.upload;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import javax.servlet.annotation.WebServlet;

/**
 * The implementation an upload library page servlet.
 *
 * @author Alex Brui
 */
@WebServlet(urlPatterns = "/library/upload/*", name = "UploadLibraryPageServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = UploadLibraryPage.class, productionMode = false)
public class UploadLibraryPageServlet extends VaadinServlet {
}
