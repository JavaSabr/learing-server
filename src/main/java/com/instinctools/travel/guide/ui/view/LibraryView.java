package com.instinctools.travel.guide.ui.view;

import com.instinctools.travel.guide.ui.component.LibraryComponent;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

import org.jetbrains.annotations.NotNull;

/**
 * The implementation a view to show library.
 *
 * @author Alex Brui.
 */
@DesignRoot
public class LibraryView extends VerticalLayout implements View {

    public LibraryView() {
        createComponents();
    }

    private void createComponents() {

        final HorizontalLayout links = new HorizontalLayout();
        links.addComponent(new Link("Upload", new ExternalResource("/library/upload/")));
        links.addComponent(new Link("Matching", new ExternalResource("/matching/")));
        links.setSpacing(true);

        addComponent(links);
        addComponent(new LibraryComponent());
    }

    @Override
    public void enter(@NotNull final ViewChangeEvent event) {
    }
}
