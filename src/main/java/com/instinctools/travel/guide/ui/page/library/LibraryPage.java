package com.instinctools.travel.guide.ui.page.library;

import com.instinctools.travel.guide.service.impl.SearchableObjectService;
import com.vaadin.annotations.Theme;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import org.jetbrains.annotations.NotNull;

/**
 * The implementation a library page.
 *
 * @author Alex Brui
 */
@Theme("mytheme")
public class LibraryPage extends UI {

    /**
     * The searchable object service.
     */
    @NotNull
    private final SearchableObjectService objectService;

    public LibraryPage() {
        this.objectService = SearchableObjectService.getInstance();
    }

    @Override
    protected void init(@NotNull final VaadinRequest vaadinRequest) {

        final Link uploadPage = new Link("Upload page", new ExternalResource("library/upload"));

        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.addComponent(uploadPage);

        setContent(layout);
    }
}
