package com.instinctools.travel.guide.ui;

import com.instinctools.travel.guide.ui.view.LibraryView;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The main page.
 *
 * @author Alex Brui
 */
@Theme("mytheme")
public class TravelGuideApplication extends UI {

    public static final Path BASE_IMAGE_PATH;

    static {

        final VaadinService current = VaadinService.getCurrent();
        final File baseDirectory = current.getBaseDirectory();
        final String absolutePath = baseDirectory.getAbsolutePath();

        BASE_IMAGE_PATH = Paths.get(absolutePath, "/WEB-INF/classes/images/test/").toAbsolutePath();
    }

    /**
     * The navigator.
     */
    private Navigator navigator;

    @Override
    protected void init(@NotNull final VaadinRequest vaadinRequest) {

        final LibraryView libraryView = new LibraryView();

        // Enable Valo menu
        addStyleName(ValoTheme.UI_WITH_MENU);
        setResponsive(true);

        final Page page = getPage();
        page.setTitle("Travel guide");
        // Create a navigator to control the views
        navigator = new Navigator(this, this);
        navigator.addView("", libraryView);

        setContent(libraryView);
    }
}
