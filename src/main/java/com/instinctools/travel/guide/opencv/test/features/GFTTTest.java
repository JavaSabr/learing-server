package com.instinctools.travel.guide.opencv.test.features;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_features2d.drawKeypoints;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags;
import org.bytedeco.javacpp.opencv_features2d.GFTTDetector;

/**
 * Example of using the Good Features to Track detector. The third example for section "Detecting Harris corners" in
 * Chapter 8, page 202. Taken this example from the https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter08/Ex3GoodFeaturesToTrack.scala
 *
 * @author Alex Brui
 */
public class GFTTTest {

    public static final Scalar COLOR = new Scalar(255, 255, 255, 0);

    public static void test() {

        final Mat image = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);

        // Compute good features to track
        final GFTTDetector detector = GFTTDetector.create(
                500 /* maximum number of corners to be returned */,
                0.01 /* quality level*/,
                10.0 /* minimum allowed distance between points*/,
                3 /* block size*/,
                false /* use Harris detector*/,
                0.04 /* Harris parameter */
        );

        final KeyPointVector keyPoints = new KeyPointVector();

        detector.detect(image, keyPoints);

        // Draw keyPoints
        final Mat canvas = new Mat();

        drawKeypoints(image, keyPoints, canvas, COLOR, DrawMatchesFlags.DEFAULT);

        imwrite(BASE_IMAGE_PATH.resolve("test-features/gftt-dest.jpg").toString(), canvas);
    }
}
