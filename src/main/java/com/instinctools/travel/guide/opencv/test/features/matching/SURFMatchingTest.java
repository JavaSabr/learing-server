package com.instinctools.travel.guide.opencv.test.features.matching;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_core.NORM_L2;
import static org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags.DEFAULT;
import static org.bytedeco.javacpp.opencv_features2d.drawMatches;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import com.instinctools.travel.guide.opencv.util.JavaCVUtils;

import org.bytedeco.javacpp.opencv_core.DMatchVector;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_features2d.BFMatcher;
import org.bytedeco.javacpp.opencv_xfeatures2d.SURF;

import java.nio.ByteBuffer;

/**
 * Example for section "Describing SURF features" in chapter 8, page 212. Computes SURF features,  extracts their
 * descriptors, and finds best matching descriptors between two images of the same object. There are a couple of tricky
 * steps, in particular sorting the descriptors. Taken this example from https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter09/Ex7DescribingSURF.scala
 *
 * @author Alex Brui
 */
public class SURFMatchingTest {

    public static final Scalar COLOR = new Scalar(255, 255, 255, 0);

    public static void test() {

        final Mat[] sources = {
                imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE),
                imread(BASE_IMAGE_PATH.resolve("source-2.jpg").toString(), IMREAD_GRAYSCALE),
        };

        // Setup SURF feature detector and descriptor.
        double hessianThreshold = 2500d;
        int nOctaves = 4;
        int nOctaveLayers = 2;
        boolean extended = true;
        boolean upright = false;

        final SURF surf = SURF.create(hessianThreshold, nOctaves, nOctaveLayers, extended, upright);

        final KeyPointVector[] keyPoints = {
                new KeyPointVector(),
                new KeyPointVector(),
        };

        final Mat[] descriptors = new Mat[2];

        // Detect SURF features and compute descriptors for both images
        for (int i = 0; i < sources.length; i++) {
            surf.detect(sources[i], keyPoints[i]);
            // Create CvMat initialized with empty pointer, using simply `new CvMat()` leads to an exception.
            descriptors[i] = new Mat();
            surf.compute(sources[i], keyPoints[i], descriptors[i]);
        }

        // Create feature matcher
        final BFMatcher matcher = new BFMatcher(NORM_L2, false);

        final DMatchVector matches = new DMatchVector();
        matcher.match(descriptors[0], descriptors[1], matches);

        System.out.println("Matched: " + matches.capacity());

        // Select only 25 best matches
        final DMatchVector bestMatches = JavaCVUtils.selectBest(matches, dMatch -> true, 25);

        // Draw best matches
        final Mat imageMatches = new Mat();

        drawMatches(sources[0], keyPoints[0], sources[1], keyPoints[1],
                bestMatches, imageMatches, new Scalar(0, 0, 255, 0), new Scalar(255, 0, 0, 0), (ByteBuffer) null, DEFAULT);

        imwrite(BASE_IMAGE_PATH.resolve("test-matching/surf-result.jpg").toString(), imageMatches);
    }


}
