package com.instinctools.travel.guide.opencv.test.features;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_features2d.drawKeypoints;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags;
import org.bytedeco.javacpp.opencv_features2d.FastFeatureDetector;

/**
 * The example for section "Detecting FAST features" in Chapter 8, page 203. Taken this example from the
 * https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter08/Ex4FAST.scala
 *
 * @author Alex Brui
 */
public class FASTTest {

    public static final Scalar COLOR = new Scalar(255, 255, 255, 0);

    public static void test() {

        final Mat image = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);

        // Compute good features to track
        final FastFeatureDetector detector = FastFeatureDetector.create(
                40 /* threshold for detection */,
                true /* non-max suppression */,
                FastFeatureDetector.TYPE_9_16
        );

        final KeyPointVector keyPoints = new KeyPointVector();

        detector.detect(image, keyPoints);

        // Draw keyPoints
        final Mat canvas = new Mat();

        drawKeypoints(image, keyPoints, canvas, COLOR, DrawMatchesFlags.DEFAULT);

        imwrite(BASE_IMAGE_PATH.resolve("test-features/fast-dest.jpg").toString(), canvas);
    }
}
