package com.instinctools.travel.guide.opencv.util;

import static java.lang.Math.min;
import static java.util.Arrays.copyOf;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.bytedeco.javacpp.opencv_core.*;
import com.instinctools.travel.guide.db.hibernate.entity.impl.InterestPoint;
import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.indexer.FloatIndexer;
import org.bytedeco.javacpp.opencv_core.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Predicate;

/**
 * Taken it from the https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/OpenCVUtils.scala
 *
 * @author Alex Brui
 */
public class JavaCVUtils {

    public static final KeyPoint[] EMPTY_KEY_POINTS = new KeyPoint[0];

    /**
     * Convert `Mat` to one where pixels are represented as 8 bit unsigned integers (`CV_8U`). It creates a copy of the
     * input image.
     *
     * @param src input image.
     * @return copy of the input with pixels values represented as 8 bit unsigned integers.
     */
    @NotNull
    public static Mat toMat8U(@NotNull final Mat src, final boolean doScaling) {

        final DoublePointer minVal = new DoublePointer(Double.MAX_VALUE);
        final DoublePointer maxVal = new DoublePointer(Double.MIN_VALUE);

        minMaxLoc(src, minVal, maxVal, null, null, new Mat());

        final double min = minVal.get(0);
        final double max = maxVal.get(0);

        double scale = 1D;
        double offset = 0D;

        if (doScaling) {
            double s = 255d / (max - min);
            scale = s;
            offset = -min * s;
        }

        final Mat dest = new Mat();
        src.convertTo(dest, CV_8U, scale, offset);

        return dest;
    }

    /**
     * Convert native vector to JVM array.
     *
     * @param keyPoints pointer to a native vector containing KeyPoints.
     */
    @NotNull
    public static KeyPoint[] toArray(@NotNull final KeyPoint keyPoints) {

        long oldPosition = keyPoints.position();

        final KeyPoint[] array = range(0, (int) keyPoints.capacity())
                .mapToObj(keyPoints::position)
                .toArray(KeyPoint[]::new);

        // Reset position explicitly to avoid issues from other uses of this position-based container.
        keyPoints.position(oldPosition);

        return array;
    }

    /**
     * Convert native vector to JVM array.
     *
     * @param matches pointer to a native vector containing DMatches.
     * @return the array of the DMath.
     */
    @NotNull
    public static DMatch[] toSortedArray(@NotNull final DMatchVector matches, @NotNull final Predicate<DMatch> test) {

        // for the simplicity of the implementation we will assume that number of key points is within Int range.
        assert matches.size() <= Integer.MAX_VALUE;

        return range(0, (int) matches.size()).mapToObj(matches::get)
                .filter(test)
                .sorted((first, second) -> first.lessThan(second) ? -1 : 1)
                .toArray(DMatch[]::new);
    }

    /**
     * Convert a Scala collection to a JavaCV "vector".
     *
     * @param src Scala collection
     * @return JavaCV/native collection
     */
    @NotNull
    public static DMatchVector toVector(@NotNull final DMatch[] src) {

        final DMatchVector dest = new DMatchVector(src.length);
        dest.put(src);

        return dest;
    }

    /**
     * Convert a list of interest points to key point vector.
     *
     * @param interestPoints the list of interest points.
     * @return the key point vector.
     */
    @NotNull
    public static KeyPointVector fromInterestPoints(@NotNull final List<InterestPoint> interestPoints) {

        final KeyPoint[] keyPoints = interestPoints.stream().
                map(JavaCVUtils::convert).
                collect(toList()).toArray(EMPTY_KEY_POINTS);

        return new KeyPointVector(keyPoints);
    }

    @NotNull
    private static KeyPoint convert(@NotNull final InterestPoint interestPoint) {
        return new KeyPoint(interestPoint.getX(), interestPoint.getY(), interestPoint.getSize(),
                interestPoint.getAngle(), interestPoint.getResponse(), interestPoint.getOctave(), -1);
    }

    /**
     * Select only the best matches from the list. Return new list.
     */
    @NotNull
    public static DMatchVector selectBest(@NotNull final DMatchVector matches, @NotNull final Predicate<DMatch> test,
                                          final int numberToSelect) {

        // Convert to sorted array
        final DMatch[] sorted = toSortedArray(matches, test);
        final DMatch[] best = copyOf(sorted, min(sorted.length, numberToSelect));

        // Select the best, and return in native vector
        return JavaCVUtils.toVector(best);
    }

    /**
     * Convert from KeyPoint to Point2D32f representation
     */
    public static Point2fVector[] toPoint2fVectorPair(@NotNull final DMatchVector matches,
                                                      @NotNull final KeyPointVector firstPoints,
                                                      @NotNull final KeyPointVector secondPoints) {

        // Extract keypoints from each match, separate Left and Right
        final int size = (int) matches.size();

        final int[] firstIndexes = new int[size];
        final int[] secondIndexes = new int[size];

        for (int i = 0; i < size; i++) {
            final DMatch dMatch = matches.get(i);
            firstIndexes[i] = dMatch.queryIdx();
            secondIndexes[i] = matches.get(i).trainIdx();
        }

        // Convert keypoints into Point2f
        final Point2fVector firstResult = new Point2fVector();
        final Point2fVector secondResult = new Point2fVector();

        KeyPoint.convert(firstPoints, firstResult, firstIndexes);
        KeyPoint.convert(secondPoints, secondResult, secondIndexes);

        return new Point2fVector[]{firstResult, secondResult,};
    }

    public static Point add(@NotNull final Point2f first, @NotNull final Point2f second) {
        return new Point((int) (first.x() + second.x()), (int) (first.y() + second.y()));
    }

    /**
     * Convert a vector of Point2f to a Mat representing a vector of Points2f.
     */
    @NotNull
    public static Mat toMat(@NotNull final Point2fVector points) {

        // Create Mat representing a vector of Points3f
        final int size = (int) points.size();

        // Argument to Mat constructor must be `Int` to mean sizes, otherwise it may be interpreted as content.
        final Mat dest = new Mat(1, size, CV_32FC2);
        final FloatIndexer indexer = dest.createIndexer();

        for (int i = 0; i < size; i++) {
            final Point2f p = points.get(i);
            indexer.put(0, i, 0, p.x());
            indexer.put(0, i, 1, p.y());
        }

        return dest;
    }

    /**
     * Convert a vector of Point2f to a Mat representing a vector of Points2f.
     */
    @NotNull
    public static Point2fVector toPointVector(@NotNull final Mat mat) {

        // Create Mat representing a vector of Points3f
        final int size = mat.cols();

        final Point2f[] array = new Point2f[size];
        final FloatIndexer indexer = mat.createIndexer();

        for (int i = 0; i < size; i++) {
            final float x = indexer.get(0, i, 0);
            final float y = indexer.get(0, i, 1);
            array[i] = new Point2f(x, y);
        }


        return new Point2fVector(array);
    }
}
