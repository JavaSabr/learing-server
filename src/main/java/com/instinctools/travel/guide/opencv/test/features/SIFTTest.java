package com.instinctools.travel.guide.opencv.test.features;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_features2d.drawKeypoints;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags;
import org.bytedeco.javacpp.opencv_xfeatures2d.SIFT;

/**
 * Example of extracting SIFT features from section "Detecting the scale-invariant SURF features" in chapter 8. Taken
 * this example from the https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter08/Ex6SIFT.scala
 *
 * @author Alex Brui
 */
public class SIFTTest {

    public static final Scalar COLOR = new Scalar(255, 255, 255, 0);

    public static void test() {

        final Mat image = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);

        // Detect SURF features.
        final KeyPointVector keyPoints = new KeyPointVector();

        int nFeatures = 0;
        int nOctaveLayers = 3;
        double contrastThreshold = 0.03;
        int edgeThreshold = 10;
        double sigma = 1.6;

        final SIFT surf = SIFT.create(nFeatures, nOctaveLayers, contrastThreshold, edgeThreshold, sigma);
        surf.detect(image, keyPoints);

        // Draw keyPoints
        final Mat canvas = new Mat();

        drawKeypoints(image, keyPoints, canvas, COLOR, DrawMatchesFlags.DRAW_RICH_KEYPOINTS);

        imwrite(BASE_IMAGE_PATH.resolve("test-features/sift-dest.jpg").toString(), canvas);
    }
}
