package com.instinctools.travel.guide.opencv;

import static org.bytedeco.javacpp.opencv_core.bitwise_and;
import static org.bytedeco.javacpp.opencv_core.compare;
import static org.bytedeco.javacpp.opencv_imgproc.circle;
import static org.bytedeco.javacpp.opencv_imgproc.cornerHarris;
import static org.bytedeco.javacpp.opencv_imgproc.dilate;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.indexer.UByteIndexer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_imgproc;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Taken this example from the https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter08/HarrisDetector.scala
 *
 * @author Alex Brui
 */
public class HarrisDetector {

    /**
     * Neighborhood size for Harris edge detector.
     */
    public static final int NEIGHBORHOOD = 3;

    /**
     * Aperture size for Harris edge detector.
     */
    public static final int APERTURE = 3;

    /**
     * Harris parameter.
     */
    public static final double K = 0.01;

    /**
     * Maximum strength for threshold computations.
     */
    private double maxStrength;

    /**
     * Image of corner strength, computed by Harris edge detector. It is created by method `detect()`.
     */
    @NotNull
    private Optional<Mat> cornerStrength;

    /**
     * Image of local corner maxima. It is created by method `detect()`.
     */
    @NotNull
    private Optional<Mat> localMax;

    public HarrisDetector() {
        this.cornerStrength = Optional.empty();
        this.localMax = Optional.empty();
        this.maxStrength = 0.0;
    }

    /**
     * Compute Harris corners.
     *
     * Results of computation can be retrieved using `getCornerMap` and `getCorners`.
     */
    public void detect(@NotNull final Mat image) {

        // Harris computations
        cornerStrength = Optional.of(new Mat());

        cornerHarris(image, cornerStrength.get(), NEIGHBORHOOD, APERTURE, K);

        // Internal threshold computation.
        //
        // We will scale corner threshold based on the maximum value in the cornerStrength image.
        // Call to cvMinMaxLoc finds min and max values in the image and assigns them to output parameters.
        // Passing back values through function parameter pointers works in C bout not on JVM.
        // We need to pass them as 1 element array, as a work around for pointers in C API.
        DoublePointer maxStrengthA = new DoublePointer(maxStrength);
        opencv_core.minMaxLoc(
                cornerStrength.get(),
                new DoublePointer(0.0) /* not used here, but required by API */,
                maxStrengthA, null, null, new Mat());

        // Read back the computed maxStrength
        maxStrength = maxStrengthA.get(0);

        // Local maxima detection.
        //
        // Dilation will replace values in the image by its largest neighbour value.
        // This process will modify all the pixels but the local maxima (and plateaus)
        final Mat dilated = new Mat();

        dilate(cornerStrength.get(), dilated, new Mat());

        localMax = Optional.of(new Mat());

        // Find maxima by detecting which pixels were not modified by dilation
        compare(cornerStrength.get(), dilated, localMax.get(), opencv_core.CMP_EQ);
    }

    /**
     * Get the corner map from the computed Harris values. Require call to `detect`.
     *
     * @throws IllegalStateException if `cornerStrength` and `localMax` are not yet computed.
     */
    @NotNull
    public Mat getCornerMap(final double qualityLevel) {

        if (!cornerStrength.isPresent() || !localMax.isPresent()) {
            throw new IllegalStateException("Need to call `detect()` before it is possible to compute corner map.");
        }

        // Threshold the corner strength
        final double threshold = qualityLevel * maxStrength;

        final Mat cornerThreshold = new Mat();
        final Mat cornerMap = new Mat();

        threshold(cornerStrength.get(), cornerThreshold, threshold, 255, opencv_imgproc.THRESH_BINARY);

        cornerThreshold.convertTo(cornerMap, opencv_core.CV_8U);

        // non-maxima suppression
        bitwise_and(cornerMap, localMax.get(), cornerMap);

        return cornerMap;
    }


    /**
     * Get the feature points from the computed Harris values. Require call to `detect`.
     */
    @NotNull
    public List<Point> getCorners(final double qualityLevel) {
        // Get the corner map
        final Mat cornerMap = getCornerMap(qualityLevel);
        // Get the corners
        return getCorners(cornerMap);
    }

    /**
     * Get the feature points vector from the computed corner map.
     */
    @NotNull
    private List<Point> getCorners(@NotNull final Mat cornerMap) {

        final UByteIndexer indexer = cornerMap.createIndexer();

        // Iterate over the pixels to obtain all feature points where matrix has non-zero values
        int width = cornerMap.cols();
        int height = cornerMap.rows();

        final List<Point> points = new ArrayList<>();

        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                final int value = indexer.get(j, i);
                if (value == 0) continue;
                points.add(new Point(i, j));
            }
        }

        return points;
    }


    /**
     * Draw circles at feature point locations on an image
     */
    public void drawOnImage(@NotNull final Mat image, @NotNull final List<Point> points) {

        final int radius = 4;
        final int thickness = 1;

        final Scalar color = new Scalar(255, 255, 255, 0);

        for (final Point point : points) {
            circle(image, point, radius, color, thickness, 8, 0);
        }
    }
}
