package com.instinctools.travel.guide.opencv.test.features.matching;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_core.minMaxLoc;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_SQDIFF;
import static org.bytedeco.javacpp.opencv_imgproc.matchTemplate;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;

/**
 * Example for section "Template Matching" in chapter 9, page 265, 2nd edition. Taken this example from
 * https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter09/Ex2TemplateMatching.scala
 *
 * @author Alex Brui
 */
public class TemplateMatchingTest {

    public static final Scalar COLOR = new Scalar(255, 255, 255, 0);

    public static void test() {

        final Mat sourceTemplate = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);
        final Mat target = imread(BASE_IMAGE_PATH.resolve("source-2.jpg").toString(), IMREAD_GRAYSCALE);

        // define a template
        final Mat template = new Mat(sourceTemplate, new Rect(120, 40, 30, 30));

        imwrite(BASE_IMAGE_PATH.resolve("test-matching/template.jpg").toString(), template);

        // define search region
        final Mat roi = new Mat(target,
                // here top half of the image
                new Rect(0, 0, target.cols(), target.rows() / 2));

        imwrite(BASE_IMAGE_PATH.resolve("test-matching/roi-1.jpg").toString(), roi);

        // perform template matching
        final Mat result = new Mat();

        matchTemplate(
                roi, // search region
                template, // template
                result, // result
                CV_TM_SQDIFF);

        // find most similar location
        final DoublePointer minVal = new DoublePointer(1);
        final DoublePointer maxVal = new DoublePointer(1);

        final Point minPt = new Point();
        final Point maxPt = new Point();

        minMaxLoc(result, minVal, maxVal, minPt, maxPt, null);

        System.out.println("minPt = [" + minPt.x() + "," + minPt.y() + "], " +
                "maxPt = [" + maxPt.x() + "," + maxPt.y() + "], minVal " + minVal.get() + ", maxVal " + maxVal.get());

        // similarity measure

        // draw rectangle at most similar location
        // at minPt in this case
        rectangle(roi, new Rect(minPt.x(), minPt.y(), template.cols(), template.rows()), COLOR);

        imwrite(BASE_IMAGE_PATH.resolve("test-matching/roi-2.jpg").toString(), roi);
    }
}
