package com.instinctools.travel.guide.opencv.test.features;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.cornerHarris;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

import com.instinctools.travel.guide.opencv.HarrisDetector;
import com.instinctools.travel.guide.opencv.util.JavaCVUtils;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;

import java.util.List;

/**
 * The second example for section "Detecting Harris corners" in Chapter 8, page 194.
 *
 * Uses Harris Corner strength image to detect well localized corners, replacing several closely located detections
 * (blurred) by a single one.
 *
 * Taken this example from the https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter08/Ex2HarrisCornerDetector.scala
 *
 * @author Alex Brui
 */
public class HarrisTest {

    public static void test() {

        final Mat image = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);
        final Mat cornerStrength = new Mat();

        cornerHarris(image, cornerStrength,
                3 /* NEIGHBORHOOD size */,
                3 /* APERTURE size */,
                0.01 /* Harris parameter */);

        final Mat harrisCorners = new Mat();

        double t = 0.0001;

        threshold(cornerStrength, harrisCorners, t, 255, THRESH_BINARY_INV);

        final Mat result = JavaCVUtils.toMat8U(harrisCorners, true);

        imwrite(BASE_IMAGE_PATH.resolve("test-features/harris-dest.jpg").toString(), result);

        final Mat image2 = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);

        final HarrisDetector detector = new HarrisDetector();
        detector.detect(image2);

        final List<Point> corners = detector.getCorners(0.01);

        detector.drawOnImage(image2, corners);

        imwrite(BASE_IMAGE_PATH.resolve("test-features/harris-dest2.jpg").toString(), image2);
    }
}
