package com.instinctools.travel.guide.opencv.test.features;

import static com.instinctools.travel.guide.ui.TravelGuideApplication.BASE_IMAGE_PATH;
import static org.bytedeco.javacpp.opencv_features2d.drawKeypoints;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_features2d.DrawMatchesFlags;
import org.bytedeco.javacpp.opencv_xfeatures2d.SURF;

/**
 * Example of extracting SURF features from section "Detecting the scale-invariant SURF features" in chapter 8. Taken
 * this example from the https://github.com/bytedeco/javacv-examples/blob/master/OpenCV2_Cookbook/src/main/scala/opencv2_cookbook/chapter08/Ex5SURF.scala
 *
 * @author Alex Brui
 */
public class SURFTest {

    public static final Scalar COLOR = new Scalar(255, 255, 255, 0);

    public static void test() {

        final Mat image = imread(BASE_IMAGE_PATH.resolve("source.jpg").toString(), IMREAD_GRAYSCALE);

        // Detect SURF features.
        final KeyPointVector keyPoints = new KeyPointVector();

        double hessianThreshold = 2500d;
        int nOctaves = 4;
        int nOctaveLayers = 2;
        boolean extended = true;
        boolean upright = false;

        final SURF surf = SURF.create(hessianThreshold, nOctaves, nOctaveLayers, extended, upright);
        surf.detect(image, keyPoints);

        // Draw keyPoints
        final Mat canvas = new Mat();

        drawKeypoints(image, keyPoints, canvas, COLOR, DrawMatchesFlags.DRAW_RICH_KEYPOINTS);

        imwrite(BASE_IMAGE_PATH.resolve("test-features/surf-dest.jpg").toString(), canvas);
    }
}
