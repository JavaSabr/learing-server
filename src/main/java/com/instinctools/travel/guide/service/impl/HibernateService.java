package com.instinctools.travel.guide.service.impl;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jetbrains.annotations.NotNull;

/**
 * The hibernate service.
 *
 * @author JavaSaBr
 */
public class HibernateService {

    @NotNull
    private static final HibernateService INSTANCE = new HibernateService();

    @NotNull
    public static HibernateService getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final SessionFactory sessionFactory;

    public HibernateService() {
        try {
            sessionFactory = new Configuration()
                    .configure("/db/hibernate/hibernate.cfg.xml")
                    .buildSessionFactory();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get a session factory.
     *
     * @return the session factory.
     */
    @NotNull
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
