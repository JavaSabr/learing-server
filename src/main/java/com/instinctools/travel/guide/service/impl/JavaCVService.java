package com.instinctools.travel.guide.service.impl;

import static java.lang.ThreadLocal.withInitial;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_xfeatures2d.SURF.create;

import org.bytedeco.javacpp.opencv_core.KeyPoint;
import org.bytedeco.javacpp.opencv_core.KeyPointVector;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_xfeatures2d.SURF;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * The service.
 *
 * @author Alex Brui
 */
public class JavaCVService {

    // Setup SURF feature detector and descriptor.
    private static final double HESSIAN_THRESHOLD = 2500d;

    private static final int N_OCTAVES = 4;
    private static final int N_OCTAVE_LAYERS = 2;

    private static final boolean EXTENDED = true;
    private static final boolean UPRIGHT = false;

    private static final ThreadLocal<SURF> SURF_THREAD_LOCAL = withInitial(() ->
            create(HESSIAN_THRESHOLD, N_OCTAVES, N_OCTAVE_LAYERS, EXTENDED, UPRIGHT));

    @NotNull
    private static final JavaCVService INSTANCE = new JavaCVService();

    @NotNull
    public static JavaCVService getInstance() {
        return INSTANCE;
    }

    public JavaCVService() {
        super();
    }

    @NotNull
    public SURF getSurf() {
        return SURF_THREAD_LOCAL.get();
    }

    /**
     * Create a description matrix for the image.
     *
     * @param file the image file.
     * @return the description matrix.
     */
    @NotNull
    public byte[] createDescriptionFor(@NotNull final Path file) {

        final Mat source = imread(file.toString(), IMREAD_GRAYSCALE);
        final Mat description = new Mat();
        final KeyPointVector keyPoints = new KeyPointVector();

        final SURF surf = SURF_THREAD_LOCAL.get();
        surf.detect(source, keyPoints);
        surf.compute(source, keyPoints, description);

        final FloatBuffer dataBuffer = description.createBuffer();
        final ByteBuffer resultBuffer = ByteBuffer.wrap(new byte[dataBuffer.limit() * 4]);

        while (dataBuffer.hasRemaining()) {
            resultBuffer.putFloat(dataBuffer.get());
        }

        source.deallocate();
        keyPoints.deallocate();
        description.deallocate();

        return resultBuffer.array();
    }

    /**
     * Detect interest points for the image.
     *
     * @param file     the image file.
     * @param consumer the key point consumer.
     */
    public void detectInterestPoints(@NotNull final Path file, @NotNull final Consumer<KeyPoint> consumer) {

        final Mat source = imread(file.toString(), IMREAD_GRAYSCALE);
        final KeyPointVector keyPoints = new KeyPointVector();

        final SURF surf = SURF_THREAD_LOCAL.get();
        surf.detect(source, keyPoints);

        keyPoints.position(0);

        for (int i = 0, length = (int) keyPoints.size(); i < length; i++) {
            consumer.accept(keyPoints.get(i));
        }

        source.deallocate();
        keyPoints.deallocate();
    }
}
