package com.instinctools.travel.guide.service.impl;

import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.close;
import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.openSession;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.size;

import com.instinctools.travel.guide.db.hibernate.entity.impl.ImageData;
import com.instinctools.travel.guide.db.hibernate.entity.impl.SearchableObject;
import com.instinctools.travel.guide.db.hibernate.util.HibernateUtil;

import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.engine.jdbc.LobCreator;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * The service to work with {@link ImageData}.
 *
 * @author Alex Brui
 */
public class ImageDataService {

    @NotNull
    private static final ImageDataService INSTANCE = new ImageDataService();

    @NotNull
    public static ImageDataService getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final JavaCVService javaCVService;

    public ImageDataService() {
        this.javaCVService = JavaCVService.getInstance();
    }

    @NotNull
    public ImageData createFor(@NotNull final Path file, @NotNull final SearchableObject object) {

        Session session = null;
        Transaction transaction = null;
        try {

            session = openSession();

            final LobCreator lobCreator = Hibernate.getLobCreator(session);
            final byte[] description = javaCVService.createDescriptionFor(file);

            final ImageData imageData = new ImageData();
            imageData.setObject(object);
            imageData.setData(lobCreator.createBlob(newInputStream(file), size(file)));
            imageData.setDescription(description);

            transaction = session.beginTransaction();
            session.save(imageData);
            transaction.commit();

            return imageData;

        } catch (final HibernateException e) {
            HibernateUtil.rollback(transaction);
            throw e;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        } finally {
            close(session);
        }
    }

    /**
     * Find the first image data of the searchable object.
     *
     * @param object the object.
     * @return the first image data or null.
     */
    @NotNull
    public ImageData findFirstWithData(@NotNull final SearchableObject object) {

        Session session = null;
        try {

            session = openSession();
            session.setHibernateFlushMode(FlushMode.MANUAL);

            final Query<ImageData> query = session.createQuery("select i from ImageData i " +
                    "where i.object = :object ", ImageData.class)
                    .setParameter("object", object)
                    .setMaxResults(1)
                    .setReadOnly(true);

            final ImageData imageData = query.uniqueResult();

            if (imageData != null) {
                imageData.getData();
            } else {
                throw new RuntimeException("not found image data for " + object);
            }

            return imageData;

        } finally {
            close(session);
        }
    }

    /**
     * Get all image data to the searchable object.
     *
     * @param object the searchable object.
     * @return the list of image data.
     */
    @NotNull
    public List<ImageData> getImageData(@NotNull final SearchableObject object) {

        Session session = null;
        try {

            session = openSession();
            session.setHibernateFlushMode(FlushMode.MANUAL);

            final Query<ImageData> query = session.createQuery("select i from ImageData i " +
                    "where i.object = :object ", ImageData.class)
                    .setParameter("object", object)
                    .setReadOnly(true);

            final List<ImageData> result = query.list();
            result.forEach(ImageData::getData);
            result.forEach(ImageData::getDescription);

            return result;

        } finally {
            close(session);
        }
    }
}
