package com.instinctools.travel.guide.service.impl;

import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.close;
import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.openSession;
import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.rollback;

import com.instinctools.travel.guide.db.hibernate.entity.impl.ImageData;
import com.instinctools.travel.guide.db.hibernate.entity.impl.InterestPoint;

import org.bytedeco.javacpp.opencv_core.KeyPoint;
import org.bytedeco.javacpp.opencv_core.Point2f;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * The service to work with {@link InterestPoint}.
 *
 * @author Alex Brui
 */
public class InterestPointService {

    @NotNull
    private static final InterestPointService INSTANCE = new InterestPointService();

    @NotNull
    public static InterestPointService getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final JavaCVService javaCVService;

    public InterestPointService() {
        this.javaCVService = JavaCVService.getInstance();
    }

    /**
     * Create a list with interest points of the image.
     *
     * @param file      the image file.
     * @param imageData the image data.
     * @return the list with interest points.
     */
    @NotNull
    public List<InterestPoint> createFor(@NotNull final Path file, @NotNull final ImageData imageData) {

        Session session = null;
        Transaction transaction = null;
        try {

            session = openSession();
            session.setHibernateFlushMode(FlushMode.MANUAL);

            final List<InterestPoint> result = new ArrayList<>();

            javaCVService.detectInterestPoints(file, keyPoint -> result.add(convert(imageData, keyPoint)));

            transaction = session.beginTransaction();
            result.forEach(session::save);
            transaction.commit();

            return result;

        } catch (final HibernateException e) {
            rollback(transaction);
            throw new RuntimeException(e);
        } finally {
            close(session);
        }
    }

    @NotNull
    private InterestPoint convert(@NotNull final ImageData imageData, @NotNull final KeyPoint keyPoint) {

        final Point2f pt = keyPoint.pt();

        return new InterestPoint(imageData, pt.x(), pt.y(), keyPoint.size(), keyPoint.angle(),
                keyPoint.response(), keyPoint.octave());
    }

    /**
     * Get all interest points for the image data.
     *
     * @param imageData the image data.
     * @return the list of interest points.
     */
    @NotNull
    public List<InterestPoint> getInterestPoints(@NotNull final ImageData imageData) {

        Session session = null;
        try {

            session = openSession();
            session.setHibernateFlushMode(FlushMode.MANUAL);

            final Query<InterestPoint> query = session.createQuery("select ip from InterestPoint ip " +
                    "where ip.imageData = :imageData", InterestPoint.class)
                    .setParameter("imageData", imageData)
                    .setReadOnly(true);

            return query.list();

        } finally {
            close(session);
        }
    }
}
