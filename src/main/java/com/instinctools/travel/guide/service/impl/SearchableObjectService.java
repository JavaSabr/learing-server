package com.instinctools.travel.guide.service.impl;

import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.close;
import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.openSession;
import static com.instinctools.travel.guide.db.hibernate.util.HibernateUtil.rollback;

import com.instinctools.travel.guide.db.hibernate.entity.impl.SearchableObject;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * The service to work with {@link SearchableObject}.
 *
 * @author Alex Brui
 */
public class SearchableObjectService {

    @NotNull
    private static final SearchableObjectService INSTANCE = new SearchableObjectService();

    @NotNull
    public static SearchableObjectService getInstance() {
        return INSTANCE;
    }

    public SearchableObjectService() {
        super();
    }

    /**
     * Get all searchable objects.
     *
     * @return the list of searchable objects.
     */
    @NotNull
    public List<SearchableObject> getAllObjects() {

        Session session = null;
        try {

            session = openSession();
            session.setHibernateFlushMode(FlushMode.MANUAL);

            final Query<SearchableObject> query = session.createQuery("select so from SearchableObject so",
                    SearchableObject.class)
                    .setReadOnly(true);

            return query.list();

        } finally {
            close(session);
        }
    }

    /**
     * Create a new searchable object.
     *
     * @param name        the name of the object.
     * @param description the description of the object.
     * @return the new searchable object.
     */
    @NotNull
    public SearchableObject create(@NotNull final String name, @NotNull final String description) {

        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(description)) {
            throw new IllegalArgumentException("The name or description can't be empty.");
        }

        Session session = null;
        Transaction transaction = null;
        try {

            session = openSession();

            final SearchableObject object = new SearchableObject(name, description);

            transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();

            return object;

        } catch (final HibernateException e) {
            rollback(transaction);
            throw e;
        } finally {
            close(session);
        }
    }
}
